﻿using IWshRuntimeLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SingleRun
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] args = Environment.GetCommandLineArgs();

            if (args.Length == 2)
            {
                try
                {
                    string fileName = args[1];
                    if (System.IO.File.Exists(fileName))
                    {
                        FileInfo fileInfo = new FileInfo(fileName);

                        string shortcutName = $"{Path.GetFileNameWithoutExtension(fileInfo.Name)}";
                        string targetPath = System.Reflection.Assembly.GetEntryAssembly().Location;
                        string arguments = $"User \"{fileInfo.DirectoryName}\" \"{fileName}\" \"\"";

                        object shDesktop = (object)"Desktop";
                        WshShell shell = new WshShell();
                        string shortcutAddress = (string)shell.SpecialFolders.Item(ref shDesktop) + @"\" + shortcutName + ".lnk";
                        IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(shortcutAddress);
                        shortcut.Description = "";
                        shortcut.TargetPath = targetPath;
                        shortcut.Arguments = arguments;
                        shortcut.Save();
                    }
                    Console.WriteLine(args[0]);
                }
                catch (Exception ex)
                {

                }
            }
            if (args.Length == 5)
            {
                //Scope
                string singleRunScope = args[1];
                //Working Directory
                string workingDirectory = args[2];
                //File Name
                string fileName = args[3];
                //Argument
                string argument = args[4];

                string message;

                var singleRunInstance = new SingleRunInstance(singleRunScope, workingDirectory, fileName, argument);
                singleRunInstance.Start(out message);
                Console.WriteLine(message);
            }

            Application.Exit();
        }
    }
}
