﻿using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Security.Cryptography;

public class Encryption
{
    //You might want to change this
    private byte[] key = { };
    private byte[] defaultSalt = { 10, 20, 30, 40, 50, 60, 70, 80 };

    public string Decrypt(string stringToDecrypt, string sEncryptionKey)
    {
        byte[] inputByteArray = new byte[stringToDecrypt.Length];
        try
        {
            key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            inputByteArray = Convert.FromBase64String(stringToDecrypt);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, defaultSalt), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            Encoding encoding = Encoding.UTF8;
            return encoding.GetString(ms.ToArray());
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    public string Encrypt(string stringToEncrypt, string sEncryptionKey)
    {
        try
        {
            key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            Byte[] inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, defaultSalt), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            return Convert.ToBase64String(ms.ToArray());
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    private static string CreateSalt(int size)
    {
        //Generate a cryptographic random number.
        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
        byte[] buff = new byte[size];
        rng.GetBytes(buff);

        // Return a Base64 string representation of the random number.
        return Convert.ToBase64String(buff);
    }

    public string EncryptText(string unencryptedText, string password, string salt = "")
    {
        // Get the bytes of the string
        byte[] bytesToBeEncrypted = Encoding.UTF8.GetBytes(unencryptedText);
        byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

        // Hash the password with SHA256
        passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

        string result = "";

        if (salt == "")
        {
            salt = CreateSalt(8);
            byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes, Convert.FromBase64String(salt));
            string saltAndEncryptedText = salt + "|" + Convert.ToBase64String(bytesEncrypted);
            result = EncryptText(saltAndEncryptedText, password, Convert.ToBase64String(defaultSalt));
        }
        else
        {
            byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes, Convert.FromBase64String(salt));
            return Convert.ToBase64String(bytesEncrypted);
        }    
        return result;
    }

    public string DecryptText(string encryptedText, string password, string salt = "")
    {
        // Get the bytes of the string
        byte[] bytesToBeDecrypted = Convert.FromBase64String(encryptedText);
        byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
        passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

        string result = "";

        if (salt=="")
        {
            byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes, defaultSalt);
            string saltAndEncryptedText = Encoding.UTF8.GetString(bytesDecrypted);
            salt = saltAndEncryptedText.Split('|')[0];
            encryptedText = saltAndEncryptedText.Split('|')[1];
            result = DecryptText(encryptedText, password, salt);
        }
        else
        {
            byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes, Convert.FromBase64String(salt));
            result = Encoding.UTF8.GetString(bytesDecrypted);
        }
        
        return result;
    }

    public byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes, byte[] saltBytes)
    {
        byte[] encryptedBytes = null;

        // Set your salt here, change it to meet your flavor:
        // The salt bytes must be at least 8 bytes.
        //if (saltBytes==null)
        //{
        //    saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
        //}

        using (MemoryStream ms = new MemoryStream())
        {
            using (RijndaelManaged AES = new RijndaelManaged())
            {
                AES.KeySize = 256;
                AES.BlockSize = 128;

                var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                AES.Key = key.GetBytes(AES.KeySize / 8);
                AES.IV = key.GetBytes(AES.BlockSize / 8);

                AES.Mode = CipherMode.CBC;

                using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                    cs.Close();
                }
                encryptedBytes = ms.ToArray();
            }
        }

        return encryptedBytes;
    }

    public byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes, byte[] saltBytes)
    {
        byte[] decryptedBytes = null;

        // Set your salt here, change it to meet your flavor:
        // The salt bytes must be at least 8 bytes.
        //if (saltBytes == null)
        //{
        //    saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
        //}

        using (MemoryStream ms = new MemoryStream())
        {
            using (RijndaelManaged AES = new RijndaelManaged())
            {
                AES.KeySize = 256;
                AES.BlockSize = 128;

                var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                AES.Key = key.GetBytes(AES.KeySize / 8);
                AES.IV = key.GetBytes(AES.BlockSize / 8);

                AES.Mode = CipherMode.CBC;

                using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                    cs.Close();
                }
                decryptedBytes = ms.ToArray();
            }
        }

        return decryptedBytes;
    }
}