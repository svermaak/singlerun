﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingleRun
{
    public class SingleRunInstance
    {
        const string PASSWORD = "54tAq%t&";
        const string REGISTRYPATH = @"SOFTWARE\ITtelligence\SingleRun";
        private string _scope;
        private string _workingDirectory = "";
        private string _fileName = "";
        private string _argument = "";
        private string _token;
        private string _value;
        Encryption _encryption = new Encryption();

        public SingleRunInstance(string Scope, string WorkingDirectory, string FileName, string Argument)
        {
            _scope = Scope;
            _workingDirectory = WorkingDirectory;
            _fileName = FileName;
            _argument = Argument;
            _token = _encryption.Encrypt($"{_workingDirectory}*****|*****{_fileName}*****|*****{_argument}", PASSWORD);
        }
        private string Value
        {
            get
            {
                try
                {
                    RegistryKey key;
                    if (_scope.ToLower() == "computer")
                    {
                        key = Registry.LocalMachine.OpenSubKey(REGISTRYPATH, true);
                        if (key == null)
                        {
                            key = Registry.LocalMachine.CreateSubKey(REGISTRYPATH, true);
                        }
                    }
                    else
                    {
                        key = Registry.CurrentUser.OpenSubKey(REGISTRYPATH, true);
                        if (key == null)
                        {
                            key = Registry.CurrentUser.CreateSubKey(REGISTRYPATH, true);
                        }
                    }
                    if (key != null)
                    {
                        return _encryption.Decrypt(key.GetValue(_token).ToString(), PASSWORD);
                    }
                }
                catch
                {

                }
                return null;
            }
            set
            {
                try
                {
                    RegistryKey key;
                    if (_scope.ToLower() == "computer")
                    {
                        key = Registry.LocalMachine.OpenSubKey(REGISTRYPATH, true);
                        if (key == null)
                        {
                            key = Registry.LocalMachine.CreateSubKey(REGISTRYPATH, true);
                        }
                    }
                    else
                    {
                        key = Registry.CurrentUser.OpenSubKey(REGISTRYPATH, true);
                        if (key == null)
                        {
                            key = Registry.CurrentUser.CreateSubKey(REGISTRYPATH, true);
                        }
                    }
                    if (key != null)
                    {
                        string encryptedToken = _encryption.Encrypt(value, PASSWORD);
                        key.SetValue(_token, encryptedToken);
                        //_value = encryptedToken;
                    }
                }
                catch
                {

                }
            }
        }
        public string PreviousProcessName
        {
            get
            {
                try
                {
                    _value = Value;
                    return _value.Split('|')[0];
                }
                catch
                {
                    return "";
                }
            }
        }
        public int PreviousProcessID
        {
            get
            {
                try
                {
                    _value = Value;
                    return int.Parse(_value.Split('|')[1]);
                }
                catch
                {
                    return 0;
                }
            }
        }
        public bool Exists() //Left as public, might be useful to call it outside of class
        {
            try
            {
                Process previousProcess = Process.GetProcessById(PreviousProcessID);
                if (PreviousProcessName == previousProcess.ProcessName)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }
        public bool Start(out string Message)
        {
            if (!Exists())
            {
                try
                {
                    string processName = "";
                    int processID =0;

                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.WorkingDirectory = _workingDirectory;
                    startInfo.FileName = _fileName;
                    startInfo.Arguments = _argument;
                    using (Process singleRunProcess = Process.Start(startInfo))
                    {
                        try
                        {
                            processName = singleRunProcess.ProcessName;
                        }
                        catch
                        {

                        }
                        try
                        {
                            processID = singleRunProcess.Id;
                        }
                        catch
                        {

                        }
                    }

                    Value = $"{processName}|{processID}";

                    Message = "Started process successfully";
                    return true;
                }
                catch (Exception ex)
                {
                    Message = "Unable to start process";
                    return false;
                }
            }
            else
            {
                Process previousProcess = Process.GetProcessById(PreviousProcessID);
                WindowHelper.BringProcessToFront(previousProcess);
                Message = "Process still running";
                return false;
            }
        }
    }
}

